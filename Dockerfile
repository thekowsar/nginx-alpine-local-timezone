FROM nginx:1.21.0-alpine

MAINTAINER  Kowsar thekowsar@gmail.com

RUN apk update
RUN apk add --no-cache tzdata
ENV TZ Asia/Dhaka


# docker build --tag=nginx-alpine:1.1 nginx-docker
# docker tag nginx-alpine:1.1 kowsar/nginx-alpine:1.1
# docker push kowsar/nginx-alpine:1.1
